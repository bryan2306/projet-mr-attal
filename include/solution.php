<?php

class Solution
{
  private $_id_Solution;
  private $_id_Incident;       
  private $_Nom_Solution;
  private $_Temps;
  
  public function __construct($idSolution, $idIncident, $nomSolution, $temps)
  {
    // N'oubliez pas qu'il faut assigner la valeur d'un attribut uniquement depuis son setter !
    $this->_id_Solution = $idSolution;
    $this->_id_Incident = $idIncident;
    $this->_Nom_Solution = $nomSolution;
    $this->_Temps = $temps;
  
  }
  
  public function setIdSolution($idSolution)
  {
      $this->_id_Solution = $idSolution;
  }
        
  public function getIdSolution()
  {
      return $this->_id_Solution;
  } 

  public function setIdIncident($idIncident)
  {
      $this->_id_Incident = $idIncident;
  }
        
  public function getIdIncident()
  {
      return $this->_id_Incident;
  }      
 
  public function setNomSolution($nomSolution)
  {
      $this->_Nom_Solution = $nomSolution;
  }

  public function getNomSolution()
  {
      return $this->_Nom_Solution;
  }

  public function setTemps($temps)
  {
      $this->_Temps = $temps;
  }

  public function getTemps()
  {
      return $this->_Temps;
  }

}

?>