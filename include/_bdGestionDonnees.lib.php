<?php

// MODIFs A FAIRE
// Ajouter en têtes 
// Voir : jeu de caractères à la connection

/** 
 * Se connecte au serveur de données                     
 * Se connecte au serveur de données à partir de valeurs
 * prédéfinies de connexion (hôte, compte utilisateur et mot de passe). 
 * Retourne l'identifiant de connexion si succès obtenu, le booléen false 
 * si problème de connexion.
 * @return resource identifiant de connexion
 */
 
function connecterServeurBD() 
{
    $PARAM_hote='localhost'; // le chemin vers le serveur
    $PARAM_port='3306';
    $PARAM_nom_bd='gestion_fonction'; // le nom de votre base de donn?s
    $PARAM_utilisateur='root'; // nom d'utilisateur pour se connecter
    $PARAM_mot_passe=''; // mot de passe de l'utilisateur pour se connecter
    $connect = new PDO('mysql:host='.$PARAM_hote.';port='.$PARAM_port.';dbname='.$PARAM_nom_bd, $PARAM_utilisateur, $PARAM_mot_passe);
    return $connect;

    //$hote = "localhost";
    // $login = "root";
    // $mdp = "";
    // return mysql_connect($hote, $login, $mdp);
}   


/** 
 * Ferme la connexion au serveur de données.
 * Ferme la connexion au serveur de données identifiée par l'identifiant de 
 * connexion $idCnx.
 * @param resource $idCnx identifiant de connexion
 * @return void  
 */
function deconnecterServeurBD($idCnx) {

}

// ************************************** PROJETS **************************************

function listerProjets($idP)
{
  $connexion = connecterServeurBD();
  
  // Si la connexion au SGBD à réussi
  if (TRUE) 
  {
      
           
      $requete="select * from T_Projet";
      if ($idP!="")
      {
          $requete=$requete." where id_Projet = '".$idP."';";
      }
      
      $jeuResultat=$connexion->query($requete); // on va chercher tous les membres de la table qu'on trie par ordre croissant

      $jeuResultat->setFetchMode(PDO::FETCH_OBJ); // on dit qu'on veut que le résultat soit récupérable sous forme d'objet     
      $i = 0;
      $ligne = $jeuResultat->fetch();

      while($ligne)
      {
          $projet[$i]['idP']=$ligne->id_Projet;
          $projet[$i]['nomP']=$ligne->Nom_Projet;
          
          $unProjet[$i] = new Projet($ligne->id_Projet,
                                     $ligne->Nom_Projet, "");
          
          $ligne=$jeuResultat->fetch();
          $i = $i + 1;
      }
  }
  $jeuResultat->closeCursor();   // fermer le jeu de résultat
  // deconnecterServeurBD($idConnexion);
  return $unProjet;
}

function ajouterProjet($idP, $nomP, &$tabErr)
{
  // Ouvrir une connexion au serveur mysql en s'identifiant
  $connexion = connecterServeurBD();
  
  // Si la connexion au SGBD à réussi
  if (TRUE) 
  {
    
    // Vérifier que l'identifiant saisie n'existe pas déja
    $requete="select * from T_Projet";
    $requete=$requete." where id_Projet = '".$idP."';"; 
    $jeuResultat=$connexion->query($requete); // on va chercher tous les membres de la table qu'on trie par ordre croissant

    $jeuResultat->setFetchMode(PDO::FETCH_OBJ); // on dit qu'on veut que le résultat soit récupérable sous forme d'objet     
    
    $ligne = $jeuResultat->fetch();
    if($ligne)
    {
      $message="Échec de l'ajout : le projet existe déjà !";
      ajouterErreur($tabErr, $message);
    }
    else
    {
      // Créer la requête d'ajout 
       $requete="insert into T_Projet"
       ."(Nom_Projet) values ('"
       .$nomP."');"; // L'id est auto incrémenté

        // Lancer la requête d'ajout 
        $ok=$connexion->query($requete); // on va chercher tous les membres de la table qu'on trie par ordre croissant
       
        // Si la requête a réussi
        if ($ok)
        {
          $message = "Le projet a été correctement ajouté";
          ajouterErreur($tabErr, $message);
        }
        else
        {
          $message = "Attention, l'ajout du projet a échoué !!!";
          ajouterErreur($tabErr, $message);
        } 

    }
    // fermer la connexion
    // deconnecterServeurBD($idConnexion);
  }
  else
  {
    $message = "Problème a la connexion <br />";
    ajouterErreur($tabErr, $message);
  }
}

function rechercherProjet($idP)
{
    $connexion = connecterServeurBD();
   
    $requete="select * from T_Projet";
    $requete=$requete." where id_Projet = '".$idP."';";  
    $jeuResultat=$connexion->query($requete); // on va chercher tous les membres de la table qu'on trie par ordre croissant 
    $jeuResultat->setFetchMode(PDO::FETCH_OBJ); // on dit qu'on veut que le résultat soit récupérable sous forme d'objet   

    $ligne = $jeuResultat->fetch();
    if($ligne)
    {

          $unProjet = new Projet($ligne->id_Projet,
                                 $ligne->Nom_Projet, "");
    }
    else 
    {
          $unProjet = new Projet("","");
      
    }
    $jeuResultat->closeCursor();   // fermer le jeu de résultat

  return $unProjet;
}

function rechercherIdProjet($idP, &$tabErr)
{
  $projet=array();
  
  $connexion = connecterServeurBD();
  
  // Création de la requête
  $requete="select * from T_Projet";
  $requete=$requete." where id_Projet = '".$idP."';"; 
  
  $jeuResultat=$connexion->query($requete); // on va chercher tous les membres de la table qu'on trie par ordre croissant

  $ligne = $jeuResultat->fetch();

  if($ligne)
  {
    $fonction['idP']=$ligne["id_Projet"];
    $fonction['nomP']=$ligne["Nom_Projet"];

  }

  return $fonction;
}

function modifierProjet($idP, $nomP, &$tabErr)
{
  // Ouvrir une connexion au serveur mysql en s'identifiant
  $connexion = connecterServeurBD();
  
  // Si la connexion au SGBD à réussi
  if ($connexion) 
  {

    // Créer la requête de modification 

       $requete="update T_Projet"
       ." SET id_Projet=".$idP.",
       Nom_Projet='".$nomP."'
       where id_Projet=".$idP.";";
    // Lancer la requête de modification
    $ok=$connexion->query($requete); 
   
    // Si la requête a échoué
    if ($ok==false)
    {
      $message = "Attention, la modification du projet a échoué !!!";
      ajouterErreur($tabErr, $message);
    }
    else 
    {
      $message = "Le projet a été correctement modifié";
      ajouterErreur($tabErr, $message); 
    } 

  }
  else
  {
    $message = "Problème a la connexion <br />";
    ajouterErreur($tabErr, $message);
  }
}

function supprimerProjet($idP,&$tabErr)
{
  // Ouvrir une connexion au serveur mysql en s'identifiant
  $connexion = connecterServeurBD();
    
    // Vérifier que l'identifiant saisie existe
    $requete="select * from T_Projet";
    $requete=$requete." where id_Projet = '".$idP."';"; 
    $jeuResultat=$connexion->query($requete); // on va chercher tous les membres de la table qu'on trie par ordre croissant
  
    //$jeuResultat->setFetchMode(PDO::FETCH_OBJ); // on dit qu'on veut que le résultat soit récupérable sous forme d'objet     
    
    $ligne = $jeuResultat->fetch();
    if($ligne)
    {
       // Créer la requête de suppression 
       $requete ="delete from T_Projet where id_Projet='".$idP."';";  
   
       // Lancer la requête de suppression
       $ok=$connexion->query($requete); // on va chercher tous les membres de la table qu'on trie par ordre croissant
          
        // Si échec de la requete
        if ($ok==FALSE)
        {
          $message = "Attention, la suppression du projet a échoué !!!";
          ajouterErreur($tabErr, $message);
        }
        else
        {
          $message = "Le projet a été correctement supprimé";
          ajouterErreur($tabErr, $message); 
        } 
  
    } 
    else
    {
      $message="Echec de la suppression : l'identifiant n'existe pas !";
      ajouterErreur($tabErr, $message);
    }  

}

// ************************************** CAHIERS DES CHARGES **************************************

function listerCahiersDesCharges($idC)
{
  $connexion = connecterServeurBD();
  
  // Si la connexion au SGBD à réussi
  if (TRUE) 
  {
      
           
      $requete="select * from T_Cahier_Des_Charges";
      if ($idC!="")
      {
          $requete=$requete." where id_Cahier_des_charges = '".$idC."';";
      }
      
      $jeuResultat=$connexion->query($requete); // on va chercher tous les membres de la table qu'on trie par ordre croissant

      $jeuResultat->setFetchMode(PDO::FETCH_OBJ); // on dit qu'on veut que le résultat soit récupérable sous forme d'objet     
      $i = 0;
      $ligne = $jeuResultat->fetch();

      while($ligne)
      {
          $cdc[$i]['idC']=$ligne->id_Cahier_des_charges;
          $cdc[$i]['idP']=$ligne->id_Projet;
          $cdc[$i]['regleM']=$ligne->Regle_Metier;
          $cdc[$i]['cheminF']=$ligne->Chemin_Fichier;
          
          $unCDC[$i] = new CahierDesCharges($ligne->id_Cahier_des_charges,
                                            $ligne->id_Projet,
                                            $ligne->Regle_Metier,
                                            $ligne->Chemin_Fichier, "");
          
          $ligne=$jeuResultat->fetch();
          $i = $i + 1;
      }
  }
  $jeuResultat->closeCursor();   // fermer le jeu de résultat
  // deconnecterServeurBD($idConnexion);
  return $unCDC;
}

function ajouterCahierDesCharges($idC, $idP, $regleM, $cheminF, &$tabErr)
{
  // Ouvrir une connexion au serveur mysql en s'identifiant
  $connexion = connecterServeurBD();
  
  // Si la connexion au SGBD à réussi
  if (TRUE) 
  {
    
    // Vérifier que l'identifiant saisie n'existe pas déja
    $requete="select * from T_Cahier_Des_Charges";
    $requete=$requete." where id_Cahier_des_charges = '".$idC."';"; 
    $jeuResultat=$connexion->query($requete); // on va chercher tous les membres de la table qu'on trie par ordre croissant

    $jeuResultat->setFetchMode(PDO::FETCH_OBJ); // on dit qu'on veut que le résultat soit récupérable sous forme d'objet     
    
    $ligne = $jeuResultat->fetch();
    if($ligne)
    {
      $message="Echec de l'ajout : le cahier des charges existe deja !";
      ajouterErreur($tabErr, $message);
    }
    else
    {
      // Créer la requête d'ajout 
       $requete="insert into T_Cahier_Des_Charges"
       ."(id_Projet, Regle_Metier, Chemin_Fichier) values ('"
       .$idP."','"
       .$regleM."','"
       .$cheminF."');"; // L'id est auto incrémenté
       echo $requete;

        // Lancer la requête d'ajout 
        $ok=$connexion->query($requete); // on va chercher tous les membres de la table qu'on trie par ordre croissant
       
        // Si la requête a réussi
        if ($ok)
        {
          $message = "Le cahier des charges a été correctement ajouté";
          ajouterErreur($tabErr, $message);
        }
        else
        {
          $message = "Attention, l'ajout du cahier des charges a échoué !!!";
          ajouterErreur($tabErr, $message);
        } 

    }
    // fermer la connexion
    // deconnecterServeurBD($idConnexion);
  }
  else
  {
    $message = "Problème a la connexion <br />";
    ajouterErreur($tabErr, $message);
  }
}

function rechercherCahierDesCharges($idC)
{
    $connexion = connecterServeurBD();
   
    $requete="select * from T_Cahier_Des_Charges";
    $requete=$requete." where id_Cahier_des_charges = '".$idC."';";  
    $jeuResultat=$connexion->query($requete); // on va chercher tous les membres de la table qu'on trie par ordre croissant 
    $jeuResultat->setFetchMode(PDO::FETCH_OBJ); // on dit qu'on veut que le résultat soit récupérable sous forme d'objet   

    $ligne = $jeuResultat->fetch();
    if($ligne)
    {

          $unCDC = new CahierDesCharges($ligne->id_Cahier_des_charges,
                                        $ligne->id_Projet,
                                        $ligne->Regle_Metier,
                                        $ligne->Chemin_Fichier, "");
    }
    else 
    {
          $unCDC = new CahierDesCharges("","","","");
      
    }
    $jeuResultat->closeCursor();   // fermer le jeu de résultat

  return $unCDC;
}

function rechercherIdCahierDesCharges($idC, &$tabErr)
{
  $cdc=array();
  
  $connexion = connecterServeurBD();
  
  // Création de la requête
  $requete="select * from T_Cahier_Des_Charges";
  $requete=$requete." where id_Cahier_des_charges = '".$idC."';"; 
  
  $jeuResultat=$connexion->query($requete); // on va chercher tous les membres de la table qu'on trie par ordre croissant

  $ligne = $jeuResultat->fetch();

  if($ligne)
  {
    $cdc['idC']=$ligne["id_Cahier_des_charges"];
    $cdc['idP']=$ligne["id_Projet"];
    $cdc['regleM']=$ligne["Regle_Metier"];
    $cdc['cheminF']=$ligne["Chemin_Fichier"];

  }

  return $cdc;
}

function modifierCahierDesCharges($idC, $idP, $regleM, $cheminF, &$tabErr)
{
  // Ouvrir une connexion au serveur mysql en s'identifiant
  $connexion = connecterServeurBD();
  
  // Si la connexion au SGBD à réussi
  if ($connexion) 
  {

    // Créer la requête de modification 

       $requete="update T_Cahier_Des_Charges"
       ." SET id_Cahier_des_charges=".$idC.",
       id_Projet=".$idP. ",
       Regle_Metier='".$regleM. "',
       Chemin_Fichier='".$cheminF."'
       where id_Cahier_des_charges=".$idC.";";
    // Lancer la requête de modification
    $ok=$connexion->query($requete); 
   
    // Si la requête a échoué
    if ($ok==false)
    {
      $message = "Attention, la modification du cahier des charges a échoué !!!";
      ajouterErreur($tabErr, $message);
    }
    else 
    {
      $message = "Le cahier des charges a été correctement modifié";
      ajouterErreur($tabErr, $message); 
    } 

  }
  else
  {
    $message = "Problème a la connexion <br />";
    ajouterErreur($tabErr, $message);
  }
}

function supprimerCahierDesCharges($idC,&$tabErr)
{
  // Ouvrir une connexion au serveur mysql en s'identifiant
  $connexion = connecterServeurBD();
    
    // Vérifier que l'identifiant saisie existe 
    $requete="select * from T_Cahier_Des_Charges";
    $requete=$requete." where id_Cahier_des_charges = '".$idC."';";  
    $jeuResultat=$connexion->query($requete); // on va chercher tous les membres de la table qu'on trie par ordre croissant
  
    //$jeuResultat->setFetchMode(PDO::FETCH_OBJ); // on dit qu'on veut que le résultat soit récupérable sous forme d'objet     
    
    $ligne = $jeuResultat->fetch();
    if($ligne)
    {
       // Créer la requête de suppression 
       $requete ="delete from T_Cahier_Des_Charges where id_Cahier_des_charges='".$idC."';";  
   
       // Lancer la requête de suppression
       $ok=$connexion->query($requete); // on va chercher tous les membres de la table qu'on trie par ordre croissant
          
        // Si échec de la requete
        if ($ok==FALSE)
        {
          $message = "Attention, la suppression du cahier des charges a échoué !!!";
          ajouterErreur($tabErr, $message);
        }
        else
        {
          $message = "Le cahier des charges a été correctement supprimé";
          ajouterErreur($tabErr, $message); 
        } 
  
    } 
    else
    {
      $message="Echec de la suppression : l'identifiant n'existe pas !";
      ajouterErreur($tabErr, $message);
    }  

}

// ************************************** FONCTIONS **************************************

function listerFonctions()
{
  $connexion = connecterServeurBD();
  
  // Si la connexion au SGBD à réussi
  if (TRUE) 
  {
      
           
      $requete="select * from T_Fonction";
 
      $jeuResultat=$connexion->query($requete); // on va chercher tous les membres de la table qu'on trie par ordre croissant

      $jeuResultat->setFetchMode(PDO::FETCH_OBJ); // on dit qu'on veut que le résultat soit récupérable sous forme d'objet     
      $i = 0;
      $ligne = $jeuResultat->fetch();

      while($ligne)
      {
          $fonction[$i]['idF']=$ligne->id_Fonction;
          $fonction[$i]['id_fichier']=$ligne->id_fichier;
          $fonction[$i]['objectif']=$ligne->Objectif;
          $fonction[$i]['codeF']=$ligne->Code_Fonction;
          $fonction[$i]['temps']=$ligne->Temps;
          
          $uneFonction[$i] = new Fonction($ligne->id_Fonction,
                                          $ligne->id_fichier,
                                          $ligne->Objectif,
                                          $ligne->Code_Fonction, 
                                          $ligne->Temps);
          
          $ligne=$jeuResultat->fetch();
          $i = $i + 1;
      }
  }
  $jeuResultat->closeCursor();   // fermer le jeu de résultat
  // deconnecterServeurBD($idConnexion);
  return $uneFonction;
}

function ajouterFonction($idF, $id_fichier, $objectif, $codeF, $temps, &$tabErr)
{
  // Ouvrir une connexion au serveur mysql en s'identifiant
  $connexion = connecterServeurBD();
  
  // Si la connexion au SGBD à réussi
  if (TRUE) 
  {
    
    // Vérifier que l'identifiant saisie n'existe pas déja
    $requete="select * from T_Fonction";
    $requete=$requete." where id_Fonction = '".$idF."';"; 
    $jeuResultat=$connexion->query($requete); // on va chercher tous les membres de la table qu'on trie par ordre croissant

    $jeuResultat->setFetchMode(PDO::FETCH_OBJ); // on dit qu'on veut que le résultat soit récupérable sous forme d'objet     
    
    $ligne = $jeuResultat->fetch();
    if($ligne)
    {
      $message="Echec de l'ajout : la fonction existe deja !";
      ajouterErreur($tabErr, $message);
    }
    else
    {
      // Créer la requête d'ajout 
       $requete="insert into T_Fonction"
       ."(id_fichier, Objectif, Code_Fonction, Temps) values ('"
       .$id_fichier."','"
       .$objectif."','"
       .$codeF."','"
       .$temps."');"; // L'id est auto incrémenté
       echo $requete;

        // Lancer la requête d'ajout 
        $ok=$connexion->query($requete); // on va chercher tous les membres de la table qu'on trie par ordre croissant
       
        // Si la requête a réussi
        if ($ok)
        {
          $message = "Le cahier des charges a été correctement ajouté";
          ajouterErreur($tabErr, $message);
        }
        else
        {
          $message = "Attention, l'ajout du cahier des charges a échoué !!!";
          ajouterErreur($tabErr, $message);
        } 

    }
    // fermer la connexion
    // deconnecterServeurBD($idConnexion);
  }
  else
  {
    $message = "Problème a la connexion <br />";
    ajouterErreur($tabErr, $message);
  }
}

function rechercherFonction($idF)
{
    $connexion = connecterServeurBD();
   
    $requete="select * from T_Fonction";
    $requete=$requete." where id_Fonction = '".$idF."';";  
    $jeuResultat=$connexion->query($requete); // on va chercher tous les membres de la table qu'on trie par ordre croissant 
    $jeuResultat->setFetchMode(PDO::FETCH_OBJ); // on dit qu'on veut que le résultat soit récupérable sous forme d'objet   

    $ligne = $jeuResultat->fetch();
    if($ligne)
    {

          $uneFonction = new Fonction($ligne->id_Fonction,
                                      $ligne->id_fichier,
                                      $ligne->Objectif,
                                      $ligne->Code_Fonction, 
                                      $ligne->Temps, "");
    }
    else 
    {
          $uneFonction = new Fonction("","","","","");
      
    }
    $jeuResultat->closeCursor();   // fermer le jeu de résultat

  return $uneFonction;
}

function rechercherIdFonction($idF, &$tabErr)
{
  $fonction=array();
  
  $connexion = connecterServeurBD();
  
  // Création de la requête
  $requete="select * from T_Fonction";
  $requete=$requete." where id_Fonction = '".$idF."';"; 
  
  $jeuResultat=$connexion->query($requete); // on va chercher tous les membres de la table qu'on trie par ordre croissant

  $ligne = $jeuResultat->fetch();

  if($ligne)
  {
    $fonction['idF']=$ligne["id_Fonction"];
    $fonction['idC']=$ligne["id_fichier"];
    $fonction['objectif']=$ligne["Objectif"];
    $fonction['codeF']=$ligne["Code_Fonction"];
    $fonction['temps']=$ligne["Temps"];

  }

  return $fonction;
}

function modifierFonction($idF, $id_fichier, $objectif, $codeF, $temps, &$tabErr)
{
  // Ouvrir une connexion au serveur mysql en s'identifiant
  $connexion = connecterServeurBD();
  
  // Si la connexion au SGBD à réussi
  if ($connexion) 
  {

    // Créer la requête de modification 

       $requete="update T_Fonction"
       ." SET id_Fonction=".$idF.",
       id_fichier=".$id_fichier. ",
       Objectif='".$objectif. "',
       Code_Fonction='".$codeF. "',
       Temps='".$temps."'
       where id_Fonction=".$idF.";";
    // Lancer la requête de modification
    $ok=$connexion->query($requete); 
   
    // Si la requête a échoué
    if ($ok==false)
    {
      $message = "Attention, la modification du cahier des charges a échoué !!!";
      ajouterErreur($tabErr, $message);
    }
    else 
    {
      $message = "Le cahier des charges a été correctement modifié";
      ajouterErreur($tabErr, $message); 
    } 

  }
  else
  {
    $message = "Problème a la connexion <br />";
    ajouterErreur($tabErr, $message);
  }
}

function supprimerFonction($idF,&$tabErr)
{
  // Ouvrir une connexion au serveur mysql en s'identifiant
  $connexion = connecterServeurBD();
    
    // Vérifier que l'identifiant saisie existe
    $requete="select * from T_Fonction";
    $requete=$requete." where id_Fonction = '".$idF."';";  
    $jeuResultat=$connexion->query($requete); // on va chercher tous les membres de la table qu'on trie par ordre croissant
  
    //$jeuResultat->setFetchMode(PDO::FETCH_OBJ); // on dit qu'on veut que le résultat soit récupérable sous forme d'objet     
    
    $ligne = $jeuResultat->fetch();
    if($ligne)
    {
       // Créer la requête de suppression 
       $requete ="delete from T_Fonction where id_Fonction='".$idF."';";  
   
       // Lancer la requête de suppression
       $ok=$connexion->query($requete); // on va chercher tous les membres de la table qu'on trie par ordre croissant
          
        // Si échec de la requete
        if ($ok==FALSE)
        {
          $message = "Attention, la suppression du cahier des charges a échoué !!!";
          ajouterErreur($tabErr, $message);
        }
        else
        {
          $message = "Le cahier des charges a été correctement supprimé";
          ajouterErreur($tabErr, $message); 
        } 
  
    } 
    else
    {
      $message="Echec de la suppression : l'identifiant n'existe pas !";
      ajouterErreur($tabErr, $message);
    }  

}

// ************************************** INCIDENTS **************************************

function listerIncidents($idI)
{
  $connexion = connecterServeurBD();
  
  // Si la connexion au SGBD à réussi
  if (TRUE) 
  {
      
           
      $requete="select * from T_Incident";
      if ($idI!="")
      {
          $requete=$requete." where id_Incident = '".$idI."';";
      }
      
      $jeuResultat=$connexion->query($requete); // on va chercher tous les membres de la table qu'on trie par ordre croissant

      $jeuResultat->setFetchMode(PDO::FETCH_OBJ); // on dit qu'on veut que le résultat soit récupérable sous forme d'objet     
      $i = 0;
      $ligne = $jeuResultat->fetch();

      while($ligne)
      {
          $incident[$i]['idI']=$ligne->id_Incident;
          $incident[$i]['nomI']=$ligne->Nom_Incident;
          $incident[$i]['temps']=$ligne->Temps;
          
          $unIncident[$i] = new Incident($ligne->id_Incident,
                                         $ligne->Nom_Incident, 
                                         $ligne->Temps, "");
          
          $ligne=$jeuResultat->fetch();
          $i = $i + 1;
      }
  }
  $jeuResultat->closeCursor();   // fermer le jeu de résultat
  // deconnecterServeurBD($idConnexion);
  return $unIncident;
}

function ajouterIncident($idI, $nomI, $temps, &$tabErr)
{
  // Ouvrir une connexion au serveur mysql en s'identifiant
  $connexion = connecterServeurBD();
  
  // Si la connexion au SGBD à réussi
  if (TRUE) 
  {
    
    // Vérifier que l'identifiant saisie n'existe pas déja
    $requete="select * from T_Incident";
    $requete=$requete." where id_Incident = '".$idI."';"; 
    $jeuResultat=$connexion->query($requete); // on va chercher tous les membres de la table qu'on trie par ordre croissant

    $jeuResultat->setFetchMode(PDO::FETCH_OBJ); // on dit qu'on veut que le résultat soit récupérable sous forme d'objet     
    
    $ligne = $jeuResultat->fetch();
    if($ligne)
    {
      $message="Échec de l'ajout : l'incident existe déjà !";
      ajouterErreur($tabErr, $message);
    }
    else
    {
      // Créer la requête d'ajout 
       $requete="insert into T_Incident"
       ."(Nom_Incident) values ('"
       .$nomI."');"; // L'id est auto incrémenté

        // Lancer la requête d'ajout 
        $ok=$connexion->query($requete); // on va chercher tous les membres de la table qu'on trie par ordre croissant
       
        // Si la requête a réussi
        if ($ok)
        {
          $message = "L'incident a été correctement ajouté";
          ajouterErreur($tabErr, $message);
        }
        else
        {
          $message = "Attention, l'ajout de l'incident a échoué !!!";
          ajouterErreur($tabErr, $message);
        } 

    }
    // fermer la connexion
    // deconnecterServeurBD($idConnexion);
  }
  else
  {
    $message = "Problème a la connexion <br />";
    ajouterErreur($tabErr, $message);
  }
}

function rechercherIncident($idI)
{
    $connexion = connecterServeurBD();
   
    $requete="select * from T_Incident";
    $requete=$requete." where id_Incident = '".$idI."';";  
    $jeuResultat=$connexion->query($requete); // on va chercher tous les membres de la table qu'on trie par ordre croissant 
    $jeuResultat->setFetchMode(PDO::FETCH_OBJ); // on dit qu'on veut que le résultat soit récupérable sous forme d'objet   

    $ligne = $jeuResultat->fetch();
    if($ligne)
    {

          $unIncident = new Incident($ligne->id_Incident,
                                     $ligne->Nom_Incident, 
                                     $ligne->Temps, "");
    }
    else 
    {
          $unIncident = new Incident("","","");
      
    }
    $jeuResultat->closeCursor();   // fermer le jeu de résultat

  return $unIncident;
}

function rechercherIdIncident($idI, &$tabErr)
{
  $incident=array();
  
  $connexion = connecterServeurBD();
  
  // Création de la requête
  $requete="select * from T_Incident";
  $requete=$requete." where id_Incident = '".$idI."';"; 
  
  $jeuResultat=$connexion->query($requete); // on va chercher tous les membres de la table qu'on trie par ordre croissant

  $ligne = $jeuResultat->fetch();

  if($ligne)
  {
    $incident['idI']=$ligne["id_Incident"];
    $incident['nomI']=$ligne["Nom_Incident"];
    $incident['temps']=$ligne["Temps"];

  }

  return $incident;
}

function modifierIncident($idI, $nomI, $temps, &$tabErr)
{
  // Ouvrir une connexion au serveur mysql en s'identifiant
  $connexion = connecterServeurBD();
  
  // Si la connexion au SGBD à réussi
  if ($connexion) 
  {

    // Créer la requête de modification 

       $requete="update T_Incident"
       ." SET id_Incident=".$idI.",
       Nom_Incident='".$nomI. "',
       Temps='".$temps."'
       where id_Incident=".$idI.";";
    // Lancer la requête de modification
    $ok=$connexion->query($requete); 
   
    // Si la requête a échoué
    if ($ok==false)
    {
      $message = "Attention, la modification du projet a échoué !!!";
      ajouterErreur($tabErr, $message);
    }
    else 
    {
      $message = "L'incident a été correctement modifié";
      ajouterErreur($tabErr, $message); 
    } 

  }
  else
  {
    $message = "Problème a la connexion <br />";
    ajouterErreur($tabErr, $message);
  }

}

function supprimerIncident($idI,&$tabErr)
{
  // Ouvrir une connexion au serveur mysql en s'identifiant
  $connexion = connecterServeurBD();
    
    // Vérifier que l'identifiant saisie existe
    $requete="select * from T_Incident";
    $requete=$requete." where id_Incident = '".$idI."';"; 
    $jeuResultat=$connexion->query($requete); // on va chercher tous les membres de la table qu'on trie par ordre croissant
  
    //$jeuResultat->setFetchMode(PDO::FETCH_OBJ); // on dit qu'on veut que le résultat soit récupérable sous forme d'objet     
    
    $ligne = $jeuResultat->fetch();
    if($ligne)
    {
       // Créer la requête de suppression 
       $requete ="delete from T_Incident where id_Incident='".$idI."';";  
   
       // Lancer la requête de suppression
       $ok=$connexion->query($requete); // on va chercher tous les membres de la table qu'on trie par ordre croissant
          
        // Si échec de la requete
        if ($ok==FALSE)
        {
          $message = "Attention, la suppression de l'incident a échoué !!!";
          ajouterErreur($tabErr, $message);
        }
        else
        {
          $message = "L'incident a été correctement supprimé";
          ajouterErreur($tabErr, $message); 
        } 
  
    } 
    else
    {
      $message="Echec de la suppression : l'identifiant n'existe pas !";
      ajouterErreur($tabErr, $message);
    }  

}

// ************************************** SOLUTIONS **************************************

function listerSolutions($idS)
{
  $connexion = connecterServeurBD();
  
  // Si la connexion au SGBD à réussi
  if (TRUE) 
  {
      
           
      $requete="select * from T_Solution";
      if ($idS!="")
      {
          $requete=$requete." where id_Solution = '".$idS."';";
      }
      
      $jeuResultat=$connexion->query($requete); // on va chercher tous les membres de la table qu'on trie par ordre croissant

      $jeuResultat->setFetchMode(PDO::FETCH_OBJ); // on dit qu'on veut que le résultat soit récupérable sous forme d'objet     
      $i = 0;
      $ligne = $jeuResultat->fetch();

      while($ligne)
      {
          $solution[$i]['idS']=$ligne->id_Solution;
          $solution[$i]['idI']=$ligne->id_Incident;
          $solution[$i]['nomS']=$ligne->Nom_Solution;
          $solution[$i]['temps']=$ligne->Temps;
          
          $uneSolution[$i] = new Solution($ligne->id_Solution,
                                          $ligne->id_Incident,
                                          $ligne->Nom_Solution, 
                                          $ligne->Temps, "");
          
          $ligne=$jeuResultat->fetch();
          $i = $i + 1;
      }
  }
  $jeuResultat->closeCursor();   // fermer le jeu de résultat
  // deconnecterServeurBD($idConnexion);
  return $uneSolution;
}

function ajouterSolution($idS, $idI, $nomS, $temps, &$tabErr)
{
  // Ouvrir une connexion au serveur mysql en s'identifiant
  $connexion = connecterServeurBD();
  
  // Si la connexion au SGBD à réussi
  if (TRUE) 
  {
    
    // Vérifier que l'identifiant saisie n'existe pas déja
    $requete="select * from T_Solution";
    $requete=$requete." where id_Solution = '".$idS."';"; 
    $jeuResultat=$connexion->query($requete); // on va chercher tous les membres de la table qu'on trie par ordre croissant

    $jeuResultat->setFetchMode(PDO::FETCH_OBJ); // on dit qu'on veut que le résultat soit récupérable sous forme d'objet     
    
    $ligne = $jeuResultat->fetch();
    if($ligne)
    {
      $message="Échec de l'ajout : la solution existe déjà !";
      ajouterErreur($tabErr, $message);
    }
    else
    {
      // Créer la requête d'ajout 
       $requete="insert into T_Solution"
       ."(id_Incident, Nom_Solution) values ('"
       .$idI."','"
       .$nomS."');"; // L'id est auto incrémenté

        // Lancer la requête d'ajout 
        $ok=$connexion->query($requete); // on va chercher tous les membres de la table qu'on trie par ordre croissant
       
        // Si la requête a réussi
        if ($ok)
        {
          $message = "La solution a été correctement ajouté";
          ajouterErreur($tabErr, $message);
        }
        else
        {
          $message = "Attention, l'ajout de la solution a échoué !!!";
          ajouterErreur($tabErr, $message);
        } 

    }
    // fermer la connexion
    // deconnecterServeurBD($idConnexion);
  }
  else
  {
    $message = "Problème a la connexion <br />";
    ajouterErreur($tabErr, $message);
  }
}

function rechercherSolution($idS)
{
    $connexion = connecterServeurBD();
   
    $requete="select * from T_Solution";
    $requete=$requete." where id_Solution = '".$idS."';";  
    $jeuResultat=$connexion->query($requete); // on va chercher tous les membres de la table qu'on trie par ordre croissant 
    $jeuResultat->setFetchMode(PDO::FETCH_OBJ); // on dit qu'on veut que le résultat soit récupérable sous forme d'objet   

    $ligne = $jeuResultat->fetch();
    if($ligne)
    {

          $uneSolution = new Solution($ligne->id_Solution,
                                      $ligne->id_Incident,
                                      $ligne->Nom_Solution, 
                                      $ligne->Temps, "");
    }
    else 
    {
          $uneSolution = new Solution("","","","");
      
    }
    $jeuResultat->closeCursor();   // fermer le jeu de résultat

  return $uneSolution;
}

function rechercherIdSolution($idS, &$tabErr)
{
  $solution=array();
  
  $connexion = connecterServeurBD();
  
  // Création de la requête
  $requete="select * from T_Solution";
  $requete=$requete." where id_Solution = '".$idS."';"; 
  
  $jeuResultat=$connexion->query($requete); // on va chercher tous les membres de la table qu'on trie par ordre croissant

  $ligne = $jeuResultat->fetch();

  if($ligne)
  {
    $solution['idS']=$ligne["id_Solution"];
    $solution['idI']=$ligne["id_Incident"];
    $solution['nomS']=$ligne["Nom_Solution"];
    $solution['temps']=$ligne["Temps"];

  }

  return $solution;
}

function modifierSolution($idS, $idI, $nomS, $temps, &$tabErr)
{
  // Ouvrir une connexion au serveur mysql en s'identifiant
  $connexion = connecterServeurBD();
  
  // Si la connexion au SGBD à réussi
  if ($connexion) 
  {

    // Créer la requête de modification 

       $requete="update T_Solution"
       ." SET id_Solution=".$idS.",
       id_Incident='".$idI. "',
       Nom_Solution='".$nomS. "',
       Temps='".$temps."'
       where id_Solution=".$idS.";";
    // Lancer la requête de modification
    $ok=$connexion->query($requete); 
   
    // Si la requête a échoué
    if ($ok==false)
    {
      $message = "Attention, la modification de la solution a échoué !!!";
      ajouterErreur($tabErr, $message);
    }
    else 
    {
      $message = "La solution a été correctement modifié";
      ajouterErreur($tabErr, $message); 
    } 

  }
  else
  {
    $message = "Problème a la connexion <br />";
    ajouterErreur($tabErr, $message);
  }
  
}

function supprimerSolution($idS,&$tabErr)
{
  // Ouvrir une connexion au serveur mysql en s'identifiant
  $connexion = connecterServeurBD();
    
    // Vérifier que l'identifiant saisie existe
    $requete="select * from T_Solution";
    $requete=$requete." where id_Solution = '".$idS."';"; 
    $jeuResultat=$connexion->query($requete); // on va chercher tous les membres de la table qu'on trie par ordre croissant
  
    //$jeuResultat->setFetchMode(PDO::FETCH_OBJ); // on dit qu'on veut que le résultat soit récupérable sous forme d'objet     
    
    $ligne = $jeuResultat->fetch();
    if($ligne)
    {
       // Créer la requête de suppression 
       $requete ="delete from T_Solution where id_Solution='".$idS."';";  
   
       // Lancer la requête de suppression
       $ok=$connexion->query($requete); // on va chercher tous les membres de la table qu'on trie par ordre croissant
          
        // Si échec de la requete
        if ($ok==FALSE)
        {
          $message = "Attention, la suppression de la solution a échoué !!!";
          ajouterErreur($tabErr, $message);
        }
        else
        {
          $message = "La solution a été correctement supprimé";
          ajouterErreur($tabErr, $message); 
        } 
  
    } 
    else
    {
      $message="Echec de la suppression : l'identifiant n'existe pas !";
      ajouterErreur($tabErr, $message);
    }  

}

function ajouterDossier($id, $nom, &$tabErr)
{
  // Ouvrir une connexion au serveur mysql en s'identifiant
  $connexion = connecterServeurBD();
  
  // Si la connexion au SGBD à réussi
  if (TRUE) 
  {
    
    // Vérifier que l'identifiant saisie n'existe pas déja
    $requete="select * from t_dossier";
    $requete=$requete." where id = '".$id."';"; 
    $jeuResultat=$connexion->query($requete); // on va chercher tous les membres de la table qu'on trie par ordre croissant

    $jeuResultat->setFetchMode(PDO::FETCH_OBJ); // on dit qu'on veut que le résultat soit récupérable sous forme d'objet     
    
    $ligne = $jeuResultat->fetch();
    if($ligne)
    {
      $message="Échec de l'ajout : le dossier existe déjà !";
      ajouterErreur($tabErr, $message);
    }
    else
    {
      // Créer la requête d'ajout 
       $requete="insert into t_dossier"
       ."(nom) values ('"
       .$nom."');"; // L'id est auto incrémenté

        // Lancer la requête d'ajout 
        $ok=$connexion->query($requete); // on va chercher tous les membres de la table qu'on trie par ordre croissant
       
        // Si la requête a réussi
        if ($ok)
        {
          $message = "Le dossier a été correctement ajouté";
          ajouterErreur($tabErr, $message);
        }
        else
        {
          $message = "Attention, l'ajout du dossier a échoué !!!";
          ajouterErreur($tabErr, $message);
        } 

    }
    // fermer la connexion
    // deconnecterServeurBD($idConnexion);
  }
  else
  {
    $message = "Problème a la connexion <br />";
    ajouterErreur($tabErr, $message);
  }
}
function ajouterFichier($id, $nom,$id_dossier,&$tabErr)
{
  // Ouvrir une connexion au serveur mysql en s'identifiant
  $connexion = connecterServeurBD();
  
  // Si la connexion au SGBD à réussi
  if (TRUE) 
  {
    
    // Vérifier que l'identifiant saisie n'existe pas déja
    $requete="select * from t_fichier";
    $requete=$requete." where id = '".$id."';"; 
    $jeuResultat=$connexion->query($requete); // on va chercher tous les membres de la table qu'on trie par ordre croissant

    $jeuResultat->setFetchMode(PDO::FETCH_OBJ); // on dit qu'on veut que le résultat soit récupérable sous forme d'objet     
    
    $ligne = $jeuResultat->fetch();
    if($ligne)
    {
      $message="Échec de l'ajout : le fichier existe déjà !";
      ajouterErreur($tabErr, $message);
    }
    else
    {
      // Créer la requête d'ajout 
       $requete="insert into t_fichier"
       ."(nom,id_dossier) values ('"
       .$nom."');"; // L'id est auto incrémenté

        // Lancer la requête d'ajout 
        $ok=$connexion->query($requete); // on va chercher tous les membres de la table qu'on trie par ordre croissant
       
        // Si la requête a réussi
        if ($ok)
        {
          $message = "Le fichier a été correctement ajouté";
          ajouterErreur($tabErr, $message);
        }
        else
        {
          $message = "Attention, l'ajout du fihcier a échoué !!!";
          ajouterErreur($tabErr, $message);
        } 

    }
    // fermer la connexion
    // deconnecterServeurBD($idConnexion);
  }
  else
  {
    $message = "Problème a la connexion <br />";
    ajouterErreur($tabErr, $message);
  }
}
/*function Fichier()
{

  $dir = 'Creat/';

  $master = new PDO('mysql:host=localhost;dbname=gestion_fonction' , 'root' , '');

     $sql = "SELECT * , f.id_dossier as dos_fichier , f.nom as fichier , d.nom as dossier FROM t_fichier  f LEFT  JOIN  t_dossier  d ON f.id_dossier = d.id";
          $result = $master->query($sql) ;

     $res = $result->fetchAll();
     foreach ($res as $key => $value) 
     {

     if($value['dos_fichier'] > 0)
    {

          echo  mkdir($dir.$value['dossier']) ;

          echo touch($dir.$value['dossier'].'/'.$value['fichier']) ;

    }
     else
     {

       echo 'test';

        echo touch($dir.$value['fichier']) ;

      }

  }

 }*/

// FIN

?>
