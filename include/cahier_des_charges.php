<?php

class CahierDesCharges
{
  private $_id_Cahier_des_charges;
  private $_id_Projet;       
  private $_Regle_Metier;
  private $_Chemin_Fichier;
  
  public function __construct($idCDC, $idProjet, $regleMetier, $cheminFichier)
  {
    // N'oubliez pas qu'il faut assigner la valeur d'un attribut uniquement depuis son setter !
    $this->_id_Cahier_des_charges = $idCDC;
    $this->_id_Projet = $idProjet;
    $this->_Regle_Metier = $regleMetier;
    $this->_Chemin_Fichier = $cheminFichier;
  
  }
  
  public function setIdCDC($idCDC)
  {
      $this->_id_Cahier_des_charges = $idCDC;
  }
        
  public function getIdCDC()
  {
      return $this->_id_Cahier_des_charges;
  } 

  public function setIdProjet($idProjet)
  {
      $this->_id_Projet = $idProjet;
  }
        
  public function getIdProjet()
  {
      return $this->_id_Projet;
  } 
      
  public function setRegleMetier($regleMetier)
  {
      $this->_Regle_Metier = $regleMetier;
  }

  public function getRegleMetier()
  {
      return $this->_Regle_Metier;
  }

  public function setCheminFichier($cheminFichier)
  {
      $this->_Chemin_Fichier = $cheminFichier;
  }

  public function getCheminFichier()
  {
      return $this->_Chemin_Fichier;
  }

}

?>