<?php

class Projet
{
  private $_id_Projet;       
  private $_Nom_Projet;
  
  public function __construct($idProjet, $nomProjet)
  {
    // N'oubliez pas qu'il faut assigner la valeur d'un attribut uniquement depuis son setter !
    $this->_id_Projet = $idProjet;
    $this->_Nom_Projet = $nomProjet;
  
  }
  
  public function setIdProjet($idProjet)
  {
      $this->_id_Projet = $idProjet;
  }
        
  public function getIdProjet()
  {
      return $this->_id_Projet;
  }      
 
  public function setNomProjet($nomProjet)
  {
      $this->_Nom_Projet = $nomProjet;
  }

  public function getNomProjet()
  {
      return $this->_Nom_Projet;
  }

}

?>
