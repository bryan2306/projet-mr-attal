<?php

class Incident
{
  private $_id_Incident;       
  private $_Nom_Incident;
  private $_Temps;
  
  public function __construct($idIncident, $nomIncident, $temps)
  {
    // N'oubliez pas qu'il faut assigner la valeur d'un attribut uniquement depuis son setter !
    $this->_id_Incident = $idIncident;
    $this->_Nom_Incident = $nomIncident;
    $this->_Temps = $temps;
  
  }
  
  public function setIdIncident($idIncident)
  {
      $this->_id_Incident = $idIncident;
  }
        
  public function getIdIncident()
  {
      return $this->_id_Incident;
  }      
 
  public function setNomIncident($nomIncident)
  {
      $this->_Nom_Incident = $nomIncident;
  }

  public function getNomIncident()
  {
      return $this->_Nom_Incident;
  }

  public function setTemps($temps)
  {
      $this->_Temps = $temps;
  }

  public function getTemps()
  {
      return $this->_Temps;
  }

}

?>