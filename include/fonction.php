<?php

class Fonction
{
  private $_id_Fonction; 
  private $_id_Fichier;      
  private $_Objectif;
  private $_Code_Fonction;
  private $_Temps;
  
  public function __construct($idFonction, $idFichier, $objectif, $codeFonction, $temps)
  {
    // N'oubliez pas qu'il faut assigner la valeur d'un attribut uniquement depuis son setter !
    $this->_id_Fonction = $idFonction;
    $this->_id_Fichier = $idFichier;
    $this->_Objectif = $objectif;
    $this->_Code_Fonction = $codeFonction;
    $this->_Temps = $temps;
  
  }
  
  public function setIdFonction($idFonction)
  {
      $this->_id_Fonction = $idFonction;
  }
        
  public function getIdFonction()
  {
      return $this->_id_Fonction;
  }

  public function setIdFichier($idFichier)
  {
      $this->_id_Fichier = $idFichier;
  }
        
  public function getIdFichier()
  {
      return $this->_id_Fichier;
  } 
       
  public function setObjectif($objectif)
  {
      $this->_Objectif = $objectif;
  }

  public function getObjectif()
  {
      return $this->_Objectif;
  }

  public function setCodeFonction($codeFonction)
  {
      $this->_Code_Fonction = $codeFonction;
  }

  public function getCodeFonction()
  {
      return $this->_Code_Fonction;
  }

  public function setTemps($temps)
  {
      $this->_Temps = $temps;
  }

  public function getTemps()
  {
      return $this->_Temps;
  }

}

?>
