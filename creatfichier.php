<?php
/** 
 * Script de contrôle et d'affichage du cas d'utilisation "Ajouter"
 * @package default
 * @todo  RAS
 */
 
$repInclude = './include/';
$repVues = './vues/';

require($repInclude . "_init.inc.php");



  ini_set('display_errors', 1);
  error_reporting(E_ALL);


  $chemin= dirname(__DIR__);
  $doss= '/GestionFonction/generate/';

  if(!file_exists($doss)){
   @mkdir($chemin.$doss);
  };

    $dir= $chemin.$doss;

    $master = new PDO('mysql:host=localhost;dbname=gestion_fonction' , 'root' , '') ; 
  
    $sql = "SELECT * , f.id_dossier as dos_fichier, f.nom as fichier, d.nom as nom_dossier FROM t_fichier f RIGHT JOIN t_dossier d ON f.id_dossier = d.id";

    $result = $master->query($sql);

    $files = $result->fetchAll(); 

          
foreach ($files as $value) {   
              
            
            if(!empty($value['nom_dossier']))
            {
                if(!file_exists($dir. $value['nom_dossier']))
                {
                   mkdir($dir. $value['nom_dossier']); 
                }
                if(!file_exists($dir. $value['nom_dossier'].'/'.$value['fichier'] ))
                {
                    touch($dir. $value['nom_dossier'].'/'.$value['fichier']) ;
                }
            }
            else
            {
                touch($dir.'/'.$value['fichier']) ;
            }
  }

    $sql2=   " SELECT nom_dossier, nom_fichier, objectif, code_fonction
               FROM t_dossier , t_fichier , t_fonction   
               WHERE t_fichier.id_dossier = t_dossier.id
               AND t_fonction.id_fichier= t_fichier.id 
               AND t_fonction.objectif= (
               SELECT Regle_Metier from t_cahier_des_charges where Regle_Metier= 'Gestion-session')
               GROUP BY nom_dossier, nom_fichier, objectif, code_fonction; ";
   
   $func= $master->query($sql2); 
   $functions= $func->fetchAll();

   foreach($functions as $value ){

     $dossier = $value['nom_dossier'];
     $fichier= $value['nom_fichier'];
     $code= explode('{' , $value['code_fonction']);
   
     $size= file_put_contents($dir.$dossier.'/'.$fichier, $code, FILE_APPEND);

     if($size===false){
      echo 'impossible d\'ecrire dans le fichier ';
      }
      else{
          echo 'ecriture reussi';
      } 
   }

/* CE CODE EN COMMENTAIRE PERMET DE CREER UN DOSSIER DANS L'ORDINATEUR
$dir = 'Creat/';

  $master = new PDO('mysql:host=localhost;dbname=gestion_fonction' , 'root' , '');

     $sql = "SELECT * , f.id_dossier as dos_fichier , f.nom as fichier , d.nom as dossier FROM t_fichier  f LEFT  JOIN  t_dossier  d ON f.id_dossier = d.id";
          $result = $master->query($sql) ;

     $res = $result->fetchAll();
     foreach ($res as $key => $value) 
     {

     if($value['dos_fichier'] > 0)
    {

            @mkdir($dir.$value['dossier']) ;

           @touch($dir.$value['dossier'].'/'.$value['fichier']) ;
             $mess= 'fichier a bien etais creer';
          echo $mess;

    }  
     else
     {
        
        $mess ='error';


         @touch($dir.$value['fichier']) ;
         echo $mess;

      }

  }
*/
// Construction de la page Rechercher
// pour l'affichage (appel des vues)
include($repVues."entete.php") ;
include($repVues."menu.php") ;
include($repVues ."erreur.php");
include($repVues."pied.php") ;
?>
  

  
 