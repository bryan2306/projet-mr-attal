<?php
/** 
 * Script de contrôle et d'affichage du cas d'utilisation "Ajouter"
 * @package default
 * @todo  RAS
 */
 
$repInclude = './include/';
$repVues = './vues/';

require($repInclude . "_init.inc.php");
require($repInclude . "incident.php");
$unIdI=lireDonneePost("idI", ""); 

if (count($_POST)==0)
{
  $etape = 1;
}
else
{
       
  $etape = 2;
  
  $unIncident=rechercherIncident($unIdI);
  
  if ($unIncident->getIdIncident() == "")
  {
    $message = "Aucun incident n'a été trouvé";   
    ajouterErreur($tabErreurs, $message); 
    
  }
  
}

// Construction de la page Rechercher
// pour l'affichage (appel des vues)
include($repVues."entete.php") ;
include($repVues."menu.php") ;
include($repVues ."erreur.php");
if ($etape==1)
{
  include($repVues."vRechercherFormIncident.php"); ;
}
else
{
  $unIncident=rechercherIncident($unIdI);
  include($repVues."vRechercherIncident.php") ;
}
include($repVues."pied.php") ;
?>
  

