

<!-- Affichage des informations sur les fleurs-->

<div class="container">

    <table class="table table-bordered table-striped table-condensed">
      <caption>
<?php
    if (isset($idC))
    {
?>
        <h3><?php echo $idC;?></h3>
<?php    
    }
?>
      </caption>
      <thead>
        <tr>
          <th>Identifiant du cahier des charges</th>
          <th>Identifiant du projet</th>
          <th>Regle du metier</th>
          <th>Chemin du projet</th>
        </tr>
      </thead>
      <tbody>  
<?php
    $i = 0;
    while($i < count($unCDC))
    { 
 ?>     
        <tr>
            <td align="right"><?php echo $unCDC->getIdCDC()?></td>
            <td align="right"><?php echo $unCDC->getIdProjet()?></td>
            <td><?php echo $unCDC->getRegleMetier()?></td>
            <td><?php echo $unCDC->getCheminFichier()?></td>
        </tr>
<?php
        $i = $i + 1;
     }
?>         
       </tbody>       
     </table>    
  </div>

 
