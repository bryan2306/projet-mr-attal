

<!-- Affichage des informations sur les fleurs-->

<div class="container">

    <table class="table table-bordered table-striped table-condensed">
      <caption>
<?php
    if (isset($idS))
    {
?>
        <h3><?php echo $idS;?></h3>
<?php    
    }
?>
      </caption>
      <thead>
        <tr>
          <th>Identifiant de la solution</th>
          <th>Identifiant de l'incident</th>
          <th>Nom de la solution</th>
          <th>Temps</th>
        </tr>
      </thead>
      <tbody>  
<?php
    $i = 0;
    while($i < count($uneSolution))
    { 
 ?>     
        <tr>
            <td align="right"><?php echo $uneSolution->getIdSolution()?></td>
            <td align="right"><?php echo $uneSolution->getIdIncident()?></td>
            <td><?php echo $uneSolution->getNomSolution()?></td>
            <td><?php echo $uneSolution->getTemps()?></td>
        </tr>
<?php
        $i = $i + 1;
     }
?>         
       </tbody>       
     </table>    
  </div>

 
