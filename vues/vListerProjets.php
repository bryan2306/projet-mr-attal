

<!-- Affichage des informations sur les fleurs-->

<div class="container">

    <table class="table table-bordered table-striped table-condensed">
      <caption>
<?php
    if (isset($idP))
    {
?>
        <h3><?php echo $idP;?></h3>
<?php    
    }
?>
      </caption>
      <thead>
        <tr>
          <th>Identifiant du projet</th>
          <th>Nom du projet</th>
        </tr>
      </thead>
      <tbody>  
<?php
    $i = 0;
    while($i < count($unProjet))
    { 
 ?>     
        <tr>
            <td align="right"><?php echo $unProjet[$i]->getIdProjet()?></td>
            <td><?php echo $unProjet[$i]->getNomProjet()?></td>
        </tr>
<?php
        $i = $i + 1;
     }
?>         
       </tbody>       
     </table>    
  </div>

 
