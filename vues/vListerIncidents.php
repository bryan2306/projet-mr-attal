

<!-- Affichage des informations sur les fleurs-->

<div class="container">

    <table class="table table-bordered table-striped table-condensed">
      <caption>
<?php
    if (isset($idI))
    {
?>
        <h3><?php echo $idI;?></h3>
<?php    
    }
?>
      </caption>
      <thead>
        <tr>
          <th>Identifiant de l'incident</th>
          <th>Nom de l'incident</th>
          <th>Temps</th>
        </tr>
      </thead>
      <tbody>  
<?php
    $i = 0;
    while($i < count($unIncident))
    { 
?>     
        <tr>
            <td align="right"><?php echo $unIncident[$i]->getIdIncident()?></td>
            <td><?php echo $unIncident[$i]->getNomIncident()?></td>
            <td><?php echo $unIncident[$i]->getTemps()?></td>
        </tr>
<?php
        $i = $i + 1;
     }
?>         
       </tbody>       
     </table>    
  </div>

 
