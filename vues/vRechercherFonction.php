

<!-- Affichage des informations sur les fleurs-->

<div class="container">

    <table class="table table-bordered table-striped table-condensed">
      <caption>
<?php
    if (isset($idF))
    {
?>
        <h3><?php echo $idF;?></h3>
<?php    
    }
?>
      </caption>
      <thead>
        <tr>
          <th>Identifiant de la fonction</th>
          <th>Identifiant du fichier</th>
          <th>Objectif</th>
          <th>Code de la fonction</th>
          <th>Temps</th>
        </tr>
      </thead>
      <tbody>  
<?php
    $i = 0;
    while($i < count($uneFonction))
    { 
 ?>     
        <tr>
            <td align="right"><?php echo $uneFonction->getIdFonction()?></td>
            <td align="right"><?php echo $uneFonction->getIdFichier()?></td>
            <td><?php echo $uneFonction->getObjectif()?></td>
            <td><?php echo $uneFonction->getCodeFonction()?></td>
            <td><?php echo $uneFonction->getTemps()?></td>
        </tr>
<?php
        $i = $i + 1;
     }
?>         
       </tbody>       
     </table>    
  </div>

 
