

<!-- Affichage des informations sur les fleurs-->

<div class="container">

    <table class="table table-bordered table-striped table-condensed">
      <caption>
<?php
    if (isset($idF))
    {
?>
        <h3><?php echo $idF;?></h3>
<?php    
    }
?>
      </caption>
      <thead>
        <tr>
          <th>Identifiant de la fonction</th>
          <th>IdentifiaNT du fichier</th>
          <th>Objectif</th>
          <th>Code de la fonction</th>
          <th>Temps</th>
        </tr>
      </thead>
      <tbody>  
<?php
    $i = 0;
   
    while($i < count($uneFonction))
    { 
 ?>     
        <tr>
            <td align="right"><?php echo $uneFonction[$i]->getIdFonction()?></td>
            <td align="right"><?php echo $uneFonction[$i]->getIdFichier()?></td>
            <td><?php echo $uneFonction[$i]->getObjectif()?></td>
            <td><?php echo $uneFonction[$i]->getCodeFonction()?></td>
            <td><?php echo $uneFonction[$i]->getTemps()?></td>
        </tr>
<?php
        $i = $i + 1;
     }
?>         
       </tbody>       
     </table>    
  </div>

 
