<script type="text/javascript">
//<![CDATA[

function valider(){
 frm=document.forms['formAjout'];
  // si le prix est positif
  if(frm.elements['prix'].value >0) {
    // les donn�es sont ok, on peut envoyer le formulaire    
    return true;
  }
  else {
    // sinon on affiche un message
    alert("Le prix doit �tre positif !");
    // et on indique de ne pas envoyer le formulaire
    return false;
  }
}
//]]>
</script>

<!--Saisie des informations dans un formulaire!-->
<div class="container">

<form name="formAjout" action="" method="post" onSubmit="return valider()">
  <fieldset>
    <legend>Entrez les donnees sur l'incident a ajouter</legend>
    <label>Identifiant de l'incident : </label> <input type="text" placeholder="Entrer l'identifiant de l'incident" name="idI" size="10" /><br />
    <label>Nom de l'incident :</label> <input type="text" placeholder="Entrer le nom de l'incident" name="nomI" size="25" /><br />
    <label>Temps :</label> <input type="text" placeholder="Entrer le temps" name="temps" size="25" /><br />
  </fieldset>
  <button type="submit" class="btn btn-primary">Enregistrer</button>
  <button type="reset" class="btn">Annuler</button>
  <p />
</form>
</div>


