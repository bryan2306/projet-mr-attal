  <!-- Navbar
    ================================================== -->
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="brand" href="./indexzz.php">Acc</a>
          <div class="nav-collapse collapse">
            <ul class="nav">
             <li class="active">
                <a href="./indexzz.php">Accueil</a>
              </li>
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">Projets <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="listerProjets.php">Lister les projets</a></li>
                            <li><a href="ajouterProjet.php">Ajouter un projet</a></li>
                            <li><a href="rechercherProjet.php">Rechercher un projet</a></li>
                            <li><a href="modifierProjet.php">Modifier un projet</a></li>
                            <li><a href="supprimerProjet.php">Supprimer un projet</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">Cahier des charges  <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="listerCahiersDesCharges.php">Lister les cahiers des charges</a></li>
                            <li><a href="ajouterCahierDesCharges.php">Ajouter un cahier des charges</a></li>
                            <li><a href="rechercherCahierDesCharges.php">Rechercher un cahier des charges</a></li>
                            <li><a href="modifierCahierDesCharges.php">Modifier un cahier des charges</a></li>
                            <li><a href="supprimerCahierDesCharges.php">Supprimer un cahier des charges</a></li>
                        </ul>  
                    </li>     
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">Fonctions  <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="listerFonctions.php">Lister les fonctions</a></li>
                            <li><a href="ajouterFonction.php">Ajouter une fonction</a></li>
                            <li><a href="rechercherFonction.php">Rechercher une fonction</a></li>
                            <li><a href="modifierFonction.php">Modifier une fonction</a></li>
                            <li><a href="supprimerFonction.php">Supprimer une fonction</a></li>
                        </ul>  
                    </li>          
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">Incidents  <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="listerIncidents.php">Lister les incidents</a></li>
                            <li><a href="ajouterIncident.php">Ajouter un incident</a></li>
                            <li><a href="rechercherIncident.php">Rechercher un incident</a></li>
                            <li><a href="modifierIncident.php">Modifier un incident</a></li>
                            <li><a href="supprimerIncident.php">Supprimer un incident</a></li>
                        </ul>  
                    </li>
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">Solutions  <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="listerSolutions.php">Lister les solutions</a></li>
                            <li><a href="ajouterSolution.php">Ajouter une solution</a></li>
                            <li><a href="rechercherSolution.php">Rechercher une solution</a></li>
                            <li><a href="modifierSolution.php">Modifier une solution</a></li>
                            <li><a href="supprimerSolution.php">Supprimer une solution</a></li>
                        </ul>  
                    </li> 
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">Dossier  <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="ajouterDossier.php">Ajouter un dossier</a></li>
                        </ul>  
                    </li>   
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">Fichier  <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="ajouterFichier.php">Ajouter un fichier</a></li>
                        </ul>  
                    </li>   
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">Create fichier  <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="creatfichier.php">creation un fichier</a></li>
                        </ul>  
                    </li>                      
                  </ul>
              </li>                           
            </ul>
          </div>
        </div>
      </div>
    </div>
</div>

