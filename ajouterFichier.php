<?php
/** 
 * Script de contrôle et d'affichage du cas d'utilisation "Ajouter"
 * @package default
 * @todo  RAS
 */
 
$repInclude = './include/';
$repVues = './vues/';

require($repInclude . "_init.inc.php");

$unid=lireDonneePost("id", "");
$unid_dossier=lireDonneePost("id_dossier", ""); 
$unnom=lireDonneePost("nom", "");


if (count($_POST)==0)
{
  $etape = 1;
}
else
{
  $etape = 2;
  ajouterFichier($unid, $unid_dossier, $unnom, $tabErreurs);
}

// Construction de la page Rechercher
// pour l'affichage (appel des vues)
include($repVues."entete.php") ;
include($repVues."menu.php") ;
include($repVues ."erreur.php");
include($repVues."vAjouterFichier.php") ;
include($repVues."pied.php") ;
?>
  
