<?php
/** 
 * Script de contrôle et d'affichage du cas d'utilisation "Ajouter"
 * @package default
 * @todo  RAS
 */
 
$repInclude = './include/';
$repVues = './vues/';

require($repInclude . "_init.inc.php");
require($repInclude . "projet.php");
$unIdP=lireDonneePost("idP", ""); 

if (count($_POST)==0)
{
  $etape = 1;
}
else
{
       
  $etape = 2;
  
  $unProjet=rechercherProjet($unIdP);
  
  if ($unProjet->getIdProjet() == "")
  {
    $message = "Aucun projet n'a été trouvé";   
    ajouterErreur($tabErreurs, $message); 
    
  }
  
}

// Construction de la page Rechercher
// pour l'affichage (appel des vues)
include($repVues."entete.php") ;
include($repVues."menu.php") ;
include($repVues ."erreur.php");
if ($etape==1)
{
  include($repVues."vRechercherFormProjet.php"); ;
}
else
{
  $unProjet=rechercherProjet($unIdP);
  include($repVues."vRechercherProjet.php") ;
}
include($repVues."pied.php") ;
?>
  

