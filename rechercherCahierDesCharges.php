<?php
/** 
 * Script de contrôle et d'affichage du cas d'utilisation "Ajouter"
 * @package default
 * @todo  RAS
 */
 
$repInclude = './include/';
$repVues = './vues/';

require($repInclude . "_init.inc.php");
require($repInclude . "cahier_des_charges.php");
$unIdC=lireDonneePost("idC", ""); 

if (count($_POST)==0)
{
  $etape = 1;
}
else
{
       
  $etape = 2;
  
  $unCDC=rechercherCahierDesCharges($unIdC);
  
  if ($unCDC->getIdCDC() == "")
  {
    $message = "Aucun cahier des charges n'a été trouvé";   
    ajouterErreur($tabErreurs, $message); 
    
  }
  
}

// Construction de la page Rechercher
// pour l'affichage (appel des vues)
include($repVues."entete.php") ;
include($repVues."menu.php") ;
include($repVues ."erreur.php");
if ($etape==1)
{
  include($repVues."vRechercherFormCahierDesCharges.php"); ;
}
else
{
  $unCDC=rechercherCahierDesCharges($unIdC);
  include($repVues."vRechercherCahierDesCharges.php") ;
}
include($repVues."pied.php") ;
?>
  

