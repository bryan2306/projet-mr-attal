<?php
/** 
 * Script de contr�le et d'affichage du cas d'utilisation "Ajouter"
 * @package default
 * @todo  RAS
 */
 
$repInclude = './include/';
$repVues = './vues/';

require($repInclude . "_init.inc.php");
  
$unIdS=lireDonneePost("idS", "");
$unIdI=lireDonneePost("idI", "");
$unNomS=lireDonneePost("nomS", "");
$unTemps=lireDonneePost("temps", "");

if (count($_POST)==0)
{
  $etape = 1;
}
else
{
  $etape = 2;
  ajouterSolution($unIdS, $unIdI, $unNomS, $unTemps, $tabErreurs);
}

// Construction de la page Rechercher
// pour l'affichage (appel des vues)
include($repVues."entete.php") ;
include($repVues."menu.php") ;
include($repVues ."erreur.php");
include($repVues."vAjouterSolution.php") ;
include($repVues."pied.php") ;
?>
  
