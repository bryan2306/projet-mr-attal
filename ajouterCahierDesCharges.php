<?php
/** 
 * Script de contr�le et d'affichage du cas d'utilisation "Ajouter"
 * @package default
 * @todo  RAS
 */
 
$repInclude = './include/';
$repVues = './vues/';

require($repInclude . "_init.inc.php");

$unIdC=lireDonneePost("idC", ""); 
$unIdP=lireDonneePost("idP", "");
$uneRegleM=lireDonneePost("regleM", "");
$unCheminF=lireDonneePost("cheminF", "");

if (count($_POST)==0)
{
  $etape = 1;
}
else
{
  $etape = 2;
  ajouterCahierDesCharges($unIdC, $unIdP, $uneRegleM, $unCheminF, $tabErreurs);
}

// Construction de la page Rechercher
// pour l'affichage (appel des vues)
include($repVues."entete.php") ;
include($repVues."menu.php") ;
include($repVues ."erreur.php");
include($repVues."vAjouterCahierDesCharges.php") ;
include($repVues."pied.php") ;
?>
  
