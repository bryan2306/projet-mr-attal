<?php
/** 
 * Script de contrôle et d'affichage du cas d'utilisation "Ajouter"
 * @package default
 * @todo  RAS
 */
 
$repInclude = './include/';
$repVues = './vues/';

require($repInclude . "_init.inc.php");
require($repInclude . "fonction.php");
$unIdF=lireDonneePost("idF", ""); 

if (count($_POST)==0)
{
  $etape = 1;
}
else
{
       
  $etape = 2;
  
  $uneFonction=rechercherFonction($unIdF);
  
  if ($uneFonction->getIdFonction() == "")
  {
    $message = "Aucune fonction n'a été trouvé";   
    ajouterErreur($tabErreurs, $message); 
    
  }
  
}

// Construction de la page Rechercher
// pour l'affichage (appel des vues)
include($repVues."entete.php") ;
include($repVues."menu.php") ;
include($repVues ."erreur.php");
if ($etape==1)
{
  include($repVues."vRechercherFormFonction.php"); ;
}
else
{
  $uneFonction=rechercherFonction($unIdF);
  include($repVues."vRechercherFonction.php") ;
}
include($repVues."pied.php") ;
?>
  

