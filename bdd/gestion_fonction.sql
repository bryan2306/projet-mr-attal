-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mar. 05 mars 2019 à 07:49
-- Version du serveur :  5.7.24
-- Version de PHP :  7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `gestion_fonction`
--

-- --------------------------------------------------------

--
-- Structure de la table `t_cahier_des_charges`
--

DROP TABLE IF EXISTS `t_cahier_des_charges`;
CREATE TABLE IF NOT EXISTS `t_cahier_des_charges` (
  `id_Cahier_des_charges` int(10) NOT NULL AUTO_INCREMENT,
  `id_Projet` int(10) NOT NULL,
  `Regle_Metier` varchar(100) NOT NULL,
  `Chemin_Fichier` varchar(100) NOT NULL,
  PRIMARY KEY (`id_Cahier_des_charges`),
  KEY `id_Projet` (`id_Projet`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `t_cahier_des_charges`
--

INSERT INTO `t_cahier_des_charges` (`id_Cahier_des_charges`, `id_Projet`, `Regle_Metier`, `Chemin_Fichier`) VALUES
(1, 1, 'Compétences en SQL', ''),
(2, 2, 'Compétences en algorithmiques et en langage JAVA', ''),
(3, 3, 'Compétence en logiciels open source', ''),
(4, 4, 'Maîtrise du langage JAVA FX et du Framework Eclipse', ''),
(5, 5, 'Maîtrise des outils de réseau et virtualisation', ''),
(6, 6, 'Compétence en logistique et gestion de projet', ''),
(7, 7, 'Maîtrise des logiciels de bureautique et d\'outils digital', ''),
(8, 1, 'Compétence en commerce et en développement Web', ''),
(9, 9, 'Maîtrise des réseaux sociaux et des outils de communication', ''),
(10, 12, 'Gestion-session', ''),
(11, 12, 'FonctionSQL', '');

-- --------------------------------------------------------

--
-- Structure de la table `t_dossier`
--

DROP TABLE IF EXISTS `t_dossier`;
CREATE TABLE IF NOT EXISTS `t_dossier` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nom` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `t_dossier`
--

INSERT INTO `t_dossier` (`id`, `nom`) VALUES
(1, 'assets'),
(2, 'bdd'),
(3, 'include'),
(4, 'vues');

-- --------------------------------------------------------

--
-- Structure de la table `t_fichier`
--

DROP TABLE IF EXISTS `t_fichier`;
CREATE TABLE IF NOT EXISTS `t_fichier` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nom` varchar(1000) NOT NULL,
  `id_dossier` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_dossier` (`id_dossier`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `t_fichier`
--

INSERT INTO `t_fichier` (`id`, `nom`, `id_dossier`) VALUES
(1, 'css', 1),
(2, 'ico', 1),
(3, 'img', 1),
(4, 'js', 1),
(5, 'baselafleurV8.sql', 2),
(6, '_bdGestionDonnees.lib.php', 3),
(7, '_bdGestionpanier.lib.php', 3),
(8, '_gestionSession.lib.php', 3),
(9, '_init.inc.php', 3),
(10, '_utilitairesEtGestionErreurs.lib.php', 3),
(11, 'entete.php', 4),
(12, 'erreur.php', 4),
(13, 'menu.php', 4),
(14, 'pied.php', 4),
(15, 'vAccueil.php', 4),
(16, 'vAjouterForm.php', 4),
(17, 'vConnexion.php', 4),
(18, 'vFleurs.php', 4),
(19, 'vInscription.php', 4),
(20, 'vPanier.php', 4),
(21, 'vRechercher.php', 4),
(22, 'vSupprimer.php', 4);

-- --------------------------------------------------------

--
-- Structure de la table `t_fonction`
--

DROP TABLE IF EXISTS `t_fonction`;
CREATE TABLE IF NOT EXISTS `t_fonction` (
  `id_Fonction` int(10) NOT NULL AUTO_INCREMENT,
  `id_fichier` int(11) NOT NULL,
  `Objectif` varchar(100) CHARACTER SET utf8 NOT NULL,
  `Code_Fonction` varchar(10000) CHARACTER SET utf8 NOT NULL,
  `Temps` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id_Fonction`),
  KEY `id_fichier` (`id_fichier`)
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `t_fonction`
--

INSERT INTO `t_fonction` (`id_Fonction`, `id_fichier`, `Objectif`, `Code_Fonction`, `Temps`) VALUES
(2, 6, 'deconnexion bdd', 'function deconnecterServeurBD($idCnx) {}', NULL),
(3, 6, 'rechercher un visiteur', 'function rechercherVisiteur($nom)\r\n{\r\n  $connexion = connecterServeurBD();\r\n\r\n  \r\n  \r\n  // Si la connexion au SGBD ?r?ssi\r\n  if (TRUE) \r\n  {\r\n      \r\n           \r\n      $requete=\"select * from visiteur where VIS_NOM= \'\".$nom.\"\';\";\r\n      \r\n     \r\n      \r\n        \r\n      \r\n      $jeuResultat=$connexion->query($requete); // on va chercher tous les membres de la table qu\'on trie par ordre croissant\r\n\r\n      $jeuResultat->setFetchMode(PDO::FETCH_OBJ); // on dit qu\'on veut que le r?ultat soit r?up?able sous forme d\'objet     \r\n      $i = 0;\r\n      $ligne = $jeuResultat->fetch();\r\n\r\n      if($ligne)\r\n      {\r\n          \r\n   \r\n          \r\n          $unVisiteur[$i] = new visiteur($ligne->VIS_MATRICULE,\r\n                            $ligne->VIS_NOM,\r\n                            $ligne->VIS_PRENOM,\r\n                            $ligne->VIS_ADRESSE,\r\n                            $ligne->VIS_CP,\r\n                            $ligne->VIS_VILLE,\r\n                            $ligne->VIS_DATEEMBAUCHE,\r\n                            $ligne->SEC_CODE,\r\n                            $ligne->LAB_CODE, \"\");\r\n\r\n          \r\n          $ligne=$jeuResultat->fetch();\r\n          $i = $i + 1;\r\n          \r\n      }\r\n  }\r\n  $jeuResultat->closeCursor();   // fermer le jeu de r?ultat\r\n  // deconnecterServeurBD($idConnexion);\r\n  return $unVisiteur;\r\n}\r\n', NULL),
(4, 6, 'lister', 'function lister()\r\n{\r\n    $connexion = connecterServeurBD();\r\n   \r\n    $requete=\"select * from produit\";\r\n    \r\n    $jeuResultat=$connexion->query($requete); // on va chercher tous les membres de la table qu\'on trie par ordre croissant\r\n  \r\n    $i = 0;\r\n    $ligne = $jeuResultat->fetch();\r\n    while($ligne)\r\n    {\r\n        $fleur[$i][\'image\']=$ligne[\'pdt_image\'];\r\n        $fleur[$i][\'ref\']=$ligne[\'pdt_ref\'];\r\n        $fleur[$i][\'designation\']=$ligne[\'pdt_designation\'];\r\n        $fleur[$i][\'prix\']=$ligne[\'pdt_prix\'];\r\n        $ligne=$jeuResultat->fetch();\r\n        $i = $i + 1;\r\n    }\r\n    $jeuResultat->closeCursor();   // fermer le jeu de résultat\r\n  \r\n  return $fleur;\r\n}', NULL),
(5, 6, 'ajouter', 'function ajouter($ref, $des, $prix, $image, $cat,&$tabErr)\r\n{\r\n  // Ouvrir une connexion au serveur mysql en s\'identifiant\r\n  $connexion = connecterServeurBD();\r\n    \r\n    // Vérifier que la référence saisie n\'existe pas déja\r\n    $requete=\"select * from produit\";\r\n    $requete=$requete.\" where pdt_ref = \'\".$ref.\"\';\"; \r\n    $jeuResultat=$connexion->query($requete); // on va chercher tous les membres de la table qu\'on trie par ordre croissant\r\n  \r\n    //$jeuResultat->setFetchMode(PDO::FETCH_OBJ); // on dit qu\'on veut que le résultat soit récupérable sous forme d\'objet     \r\n    \r\n    $ligne = $jeuResultat->fetch();\r\n    if($ligne)\r\n    {\r\n      $message=\"Echec de l\'ajout : la référence existe déjà !\";\r\n      ajouterErreur($tabErr, $message);\r\n    }\r\n    else\r\n    {\r\n      // Créer la requête d\'ajout \r\n       $requete=\"insert into produit\"\r\n       .\"(pdt_ref,pdt_designation,pdt_prix,pdt_image, pdt_categorie) values (\'\"\r\n       .$ref.\"\',\'\"\r\n       .$des.\"\',\"\r\n       .$prix.\",\'\"\r\n       .$image.\"\',\'\"\r\n       .$cat.\"\');\";\r\n     \r\n        // Lancer la requête d\'ajout \r\n        $ok=$connexion->query($requete); // on va chercher tous les membres de la table qu\'on trie par ordre croissant\r\n          \r\n        // Si échec de la requete\r\n        if ($ok==FALSE)\r\n        {\r\n          $message = \"Attention, l\'ajout de la fleur a échoué !!!\";\r\n          ajouterErreur($tabErr, $message);\r\n        } \r\n  \r\n    }\r\n}', NULL),
(6, 6, 'rechercher', 'function rechercher($des)\r\n{\r\n    $connexion = connecterServeurBD();\r\n    \r\n    $fleur = array();\r\n   \r\n    $requete=\"select * from produit\";\r\n      $requete=$requete.\" where pdt_designation=\'\".$des.\"\';\";\r\n    $jeuResultat=$connexion->query($requete); // on va chercher tous les membres de la table qu\'on trie par ordre croissant\r\n  \r\n    $i = 0;\r\n    $ligne = $jeuResultat->fetch();\r\n    while($ligne)\r\n    {\r\n        $fleur[$i][\'image\']=$ligne[\'pdt_image\'];\r\n        $fleur[$i][\'ref\']=$ligne[\'pdt_ref\'];\r\n        $fleur[$i][\'designation\']=$ligne[\'pdt_designation\'];\r\n        $fleur[$i][\'prix\']=$ligne[\'pdt_prix\'];\r\n        $ligne=$jeuResultat->fetch();\r\n        $i = $i + 1;\r\n    }\r\n    $jeuResultat->closeCursor();   // fermer le jeu de résultat\r\n  \r\n  return $fleur;\r\n}\r\n', NULL),
(7, 6, 'rechercher reference', 'function rechercherRef($uneRef,&$tabErr)\r\n{\r\n  $fleur=array();\r\n  \r\n  $connexion = connecterServeurBD();\r\n  \r\n  // Création de la requête\r\n  $requete=\"select * from produit\";\r\n  $requete=$requete.\" where pdt_ref = \'\".$uneRef.\"\';\"; \r\n  \r\n  $jeuResultat=$connexion->query($requete); // on va chercher tous les membres de la table qu\'on trie par ordre croissant\r\n\r\n  $ligne = $jeuResultat->fetch();\r\n\r\n  if($ligne)\r\n  {\r\n    $fleur[\'image\']=$ligne[\"pdt_image\"];\r\n    $fleur[\'ref\']=$ligne[\"pdt_ref\"];\r\n    $fleur[\'designation\']=$ligne[\"pdt_designation\"];\r\n    $fleur[\'prix\']=$ligne[\"pdt_prix\"];\r\n    $fleur[\'categorie\']=$ligne[\"pdt_categorie\"];\r\n  }\r\n\r\n  return $fleur;\r\n}\r\n', NULL),
(8, 6, 'supprimer', 'function supprimer($ref,&$tabErr)\r\n{\r\n  // Ouvrir une connexion au serveur mysql en s\'identifiant\r\n  $connexion = connecterServeurBD();\r\n    \r\n    // Vérifier que la référence saisie existe\r\n    $requete=\"select * from produit\";\r\n    $requete=$requete.\" where pdt_ref = \'\".$ref.\"\';\"; \r\n    $jeuResultat=$connexion->query($requete); // on va chercher tous les membres de la table qu\'on trie par ordre croissant\r\n  \r\n    //$jeuResultat->setFetchMode(PDO::FETCH_OBJ); // on dit qu\'on veut que le résultat soit récupérable sous forme d\'objet     \r\n    \r\n    $ligne = $jeuResultat->fetch();\r\n    if($ligne)\r\n    {\r\n       // Créer la requête de suppression \r\n       $requete =\"delete from produit where pdt_ref=\'\".$ref.\"\';\";        \r\n       // Lancer la requête de suppression\r\n       $ok=$connexion->query($requete); // on va chercher tous les membres de la table qu\'on trie par ordre croissant\r\n          \r\n        // Si échec de la requete\r\n        if ($ok==FALSE)\r\n        {\r\n          $message = \"Attention, la suppression de la fleur a échoué !!!\";\r\n          ajouterErreur($tabErr, $message);\r\n        } \r\n  \r\n    } \r\n    else\r\n    {\r\n      $message=\"Echec de la suppression : la référence n\'existe pas !\";\r\n      ajouterErreur($tabErr, $message);\r\n    }  \r\n}', NULL),
(9, 6, 'modifier', 'function modifier($ref, $des, $prix, $image, $cat,&$tabErr)\r\n{\r\n  // Ouvrir une connexion au serveur mysql en s\'identifiant\r\n  $connexion = connecterServeurBD();\r\n  \r\n  // Si la connexion au SGBD à réussi\r\n  if ($connexion) \r\n  {\r\n\r\n    // Créer la requête de modification \r\n\r\n    $requete =\"update produit set pdt_designation =\'\".$des.\r\n      \"\',pdt_prix=\".$prix.\r\n      \",pdt_image=\'\".$image.\r\n      \"\',pdt_categorie=\'\".$cat.\r\n      \"\' where pdt_ref=\'\".$ref.\"\';\";\r\n\r\n    // Lancer la requête de modification\r\n    $ok=$connexion->query($requete); \r\n   \r\n    // Si la requête a échoué\r\n    if ($ok==false)\r\n    {\r\n      $message = \"Attention, la modification de la fleur a échoué !!!\";\r\n      ajouterErreur($tabErr, $message);\r\n    } \r\n\r\n  }\r\n  else\r\n  {\r\n    $message = \"problème à la connexion <br />\";\r\n    ajouterErreur($tabErr, $message);\r\n  }\r\n}', NULL),
(10, 6, 'rechercher utilisateur', 'function rechercherUtilisateur($log,$mdp,&$tabErr)\r\n{\r\n    $connexion = connecterServeurBD();\r\n    \r\n    $utilisateur = array();\r\n   \r\n    $requete=\"select * from utilisateur where nom=\'\".$log.\"\' and mdp=\'\".$mdp.\"\';\";\r\n    $jeuResultat=$connexion->query($requete); // on va chercher tous les membres de la table qu\'on trie par ordre croissant\r\n    $ligne = $jeuResultat->fetch();\r\n\r\n  if($ligne)\r\n  {\r\n    $utilisateur[\'log\']=$ligne[\"nom\"];\r\n    $utilisateur[\'mdp\']=$ligne[\"mdp\"];\r\n    $utilisateur[\'cat\']=$ligne[\"cat\"];\r\n\r\n  }\r\n   \r\n    $jeuResultat->closeCursor();   // fermer le jeu de résultat\r\n     \r\n  return $utilisateur;\r\n}\r\n', NULL),
(11, 8, 'gestion-session', 'function initSession() {\r\n    session_start();\r\n}\r\n', NULL),
(12, 8, 'gestion-session', 'function obtenirIdUserConnecte() {\r\n    $ident=\"\";\r\n    if ( isset($_SESSION[\"loginUser\"]) ) {\r\n        $ident = (isset($_SESSION[\"idUser\"])) ? $_SESSION[\"idUser\"] : \'\';   \r\n    }  \r\n    return $ident ;\r\n}', NULL),
(13, 8, 'gestion-session', 'function connecter($login, $mdp,$cat) {\r\n    $_SESSION[\"login\"] = $login;\r\n    $_SESSION[\"mdp\"] = $mdp;\r\n    $_SESSION[\"cat\"] = $cat;\r\n\r\n}\r\n', NULL),
(14, 8, 'gestion-session', 'function deconnecter() {\r\n    unset($_SESSION[\"login\"]);\r\n    unset($_SESSION[\"mdp\"]);\r\n    unset($_SESSION[\"cat\"]);\r\n}\r\n', NULL),
(15, 8, 'gestion-session', 'function estConnecte() \r\n{\r\n$ok = false;\r\nif (isset($_SESSION[\"login\"]))\r\n{\r\n  $ok = true; \r\n}\r\nreturn $ok;\r\n}', NULL),
(16, 8, 'gestion-session', 'function estConnecteclient() \r\n{\r\n$ok = false;\r\nif (isset($_SESSION[\"login\"]) and ($_SESSION[\"cat\"]==\"client\"))\r\n{\r\n  $ok = true; \r\n}\r\nreturn $ok;\r\n}', NULL),
(17, 10, 'gestion-erreur', 'function obtenirLibelleMois($unNoMois) {\r\n    $tabLibelles = array(1=>\"Janvier\", \r\n                            \"Février\", \"Mars\", \"Avril\", \"Mai\", \"Juin\", \"Juillet\",\r\n                            \"Août\", \"Septembre\", \"Octobre\", \"Novembre\", \"Décembre\");\r\n    $libelle=\"\";\r\n    if ( $unNoMois >=1 && $unNoMois <= 12 ) {\r\n        $libelle = $tabLibelles[$unNoMois];\r\n    }\r\n    return $libelle;\r\n}', NULL),
(18, 10, 'gestion-erreur', 'function estDate($date) {\r\n	$tabDate = explode(\'/\',$date);\r\n	if (count($tabDate) != 3) {\r\n	    $dateOK = false;\r\n    }\r\n    elseif (!verifierEntiersPositifs($tabDate)) {\r\n        $dateOK = false;\r\n    }\r\n    elseif (!checkdate($tabDate[1], $tabDate[0], $tabDate[2])) {\r\n        $dateOK = false;\r\n    }\r\n    else {\r\n        $dateOK = true;\r\n    }\r\n	return $dateOK;\r\n}', NULL),
(19, 10, 'gestion-erreur', 'function convertirDateFrancaisVersAnglais($date){\r\n	@list($jour,$mois,$annee) = explode(\'/\',$date);\r\n	return date(\"Y-m-d\", mktime(0, 0, 0, $mois, $jour, $annee));\r\n}\r\n', NULL),
(20, 10, 'gestion-erreur', 'function convertirDateAnglaisVersFrancais($date){\r\n    @list($annee,$mois,$jour) = explode(\'-\',$date);\r\n	return date(\"d/m/Y\", mktime(0, 0, 0, $mois, $jour, $annee));\r\n}', NULL),
(21, 10, 'gestion-erreur', 'function estDansAnneeEcoulee($date) {\r\n	$dateAnglais = convertirDateFrancaisVersAnglais($date);\r\n	$dateDuJourAnglais = date(\"Y-m-d\");\r\n	$dateDuJourMoinsUnAnAnglais = date(\"Y-m-d\", mktime(0, 0, 0, date(\"m\"), date(\"d\"), date(\"Y\") - 1));\r\n	return ($dateAnglais >= $dateDuJourMoinsUnAnAnglais) && ($dateAnglais <= $dateDuJourAnglais);\r\n}', NULL),
(22, 10, 'gestion-erreur', 'function estEntierPositif($valeur) {\r\n    return preg_match(\"/[^0-9]/\", $valeur) == 0;\r\n}', NULL),
(23, 10, 'gestion-erreur', 'function verifierEntiersPositifs($lesValeurs){\r\n    $ok = true;     \r\n    foreach ( $lesValeurs as $val ) {\r\n        if ($val==\"\" || ! estEntierPositif($val) ) {\r\n            $ok = false;\r\n        }\r\n    }\r\n    return $ok; \r\n}', NULL),
(24, 10, 'gestion-erreur', 'function lireDonneeUrl($nomDonnee, $valDefaut=\"\") {\r\n    if ( isset($_GET[$nomDonnee]) ) {\r\n        $val = $_GET[$nomDonnee];\r\n    }\r\n    else {\r\n        $val = $valDefaut;\r\n    }\r\n    return $val;\r\n}', NULL),
(25, 10, 'gestion-erreur', 'function ajouterErreur(&$tabErr,$msg) {\r\n    $tabErr[count($tabErr)]=$msg;\r\n}\r\n', NULL),
(26, 10, 'gestion-erreur', 'function nbErreurs($tabErr) {\r\n    return count($tabErr);\r\n}', NULL),
(27, 10, 'gestion-erreur', 'function toStringErreurs($tabErr) {\r\n    $str = \'<div class=\"erreur\">\';\r\n    $str .= \'<ul>\';\r\n    foreach($tabErr as $erreur){\r\n        $str .= \'<li>\' . $erreur . \'</li>\';\r\n	}\r\n    $str .= \'</ul>\';\r\n    $str .= \'</div>\';\r\n    return $str;\r\n} ', NULL),
(28, 10, 'gestion-erreur', 'function filtrerChainePourNavig($str) {\r\n    return htmlspecialchars($str, ENT_QUOTES, \'UTF-8\');\r\n}', NULL),
(29, 10, 'gestion-erreur', 'function verifierLigneFraisHF($date, $libelle, $montant, &$tabErrs) \r\n', NULL),
(30, 10, 'gestion-erreur', 'function ajouterpanier($ref) \r\n\r\n{\r\n\r\n	if(!isset($_SESSION[\'panier\']))\r\n	{\r\n		$_SESSION[\'panier\']=array();\r\n	}\r\n\r\n	 $i=count($_SESSION[\'panier\']);\r\n	 $_SESSION[\'panier\'][$i]=$ref;\r\n	 var_dump($_SESSION[\'panier\']);\r\n\r\n}\r\n', NULL),
(31, 10, 'gestion-erreur', 'function obtenirPanier()\r\n{\r\n	return $_SESSION[\'panier\'];\r\n} ', NULL),
(32, 10, 'gestion-erreur', 'function comptepanier()\r\n{\r\n\r\n	if(isset($_SESSION[\'panier\']))\r\n	{\r\n		$compte=count($_SESSION[\'panier\']);\r\n	}\r\n	return $compte;\r\n}\r\n', NULL),
(33, 7, 'gestion-panier', 'function ajouterpanier($ref) \r\n\r\n{\r\n\r\n	if(!isset($_SESSION[\'panier\']))\r\n	{\r\n		$_SESSION[\'panier\']=array();\r\n	}\r\n\r\n	 $i=count($_SESSION[\'panier\']);\r\n	 $_SESSION[\'panier\'][$i]=$ref;\r\n	 var_dump($_SESSION[\'panier\']);\r\n\r\n}\r\n', NULL),
(34, 7, 'gestion-panier', 'function obtenirPanier()\r\n{\r\n	return $_SESSION[\'panier\'];\r\n} ', NULL),
(35, 7, 'gestion-panier', 'function comptepanier()\r\n{\r\n\r\n	if(isset($_SESSION[\'panier\']))\r\n	{\r\n		$compte=count($_SESSION[\'panier\']);\r\n	}\r\n	return $compte;\r\n}\r\n', NULL),
(36, 7, 'gestion-panier', '\r\nfunction rechercherFleur($ref)\r\n{\r\n    $connexion = connecterServeurBD();\r\n    \r\n    $fleur = array();\r\n   \r\n    $requete=\"select * from produit\";\r\n      $requete=$requete.\" where pdt_ref=\'\".$ref.\"\';\";\r\n    $jeuResultat=$connexion->query($requete); // on va chercher tous les membres de la table qu\'on trie par ordre croissant\r\n  \r\n    $i = 0;\r\n    $ligne = $jeuResultat->fetch();\r\n    while($ligne)\r\n    {\r\n        $fleur[$i][\'image\']=$ligne[\'pdt_image\'];\r\n        $fleur[$i][\'ref\']=$ligne[\'pdt_ref\'];\r\n        $fleur[$i][\'designation\']=$ligne[\'pdt_designation\'];\r\n        $fleur[$i][\'prix\']=$ligne[\'pdt_prix\'];\r\n        $ligne=$jeuResultat->fetch();\r\n        $i = $i + 1;\r\n    }\r\n    $jeuResultat->closeCursor();   // fermer le jeu de résultat\r\n  \r\n  return $fleur;\r\n}', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `t_incident`
--

DROP TABLE IF EXISTS `t_incident`;
CREATE TABLE IF NOT EXISTS `t_incident` (
  `id_Incident` int(10) NOT NULL AUTO_INCREMENT,
  `Nom_Incident` varchar(100) NOT NULL,
  `Temps` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id_Incident`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `t_incident`
--

INSERT INTO `t_incident` (`id_Incident`, `Nom_Incident`, `Temps`) VALUES
(1, 'Pannes de serveurs', NULL),
(2, 'PC impossibles à démarrer', NULL),
(3, 'Serveurs de bases de données inactifs', NULL),
(4, 'Erreur de codage', NULL),
(5, 'Connexion impossible', ''),
(6, 'Réseau internet inaccessible', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `t_projet`
--

DROP TABLE IF EXISTS `t_projet`;
CREATE TABLE IF NOT EXISTS `t_projet` (
  `id_Projet` int(10) NOT NULL AUTO_INCREMENT,
  `Nom_Projet` varchar(100) NOT NULL,
  PRIMARY KEY (`id_Projet`),
  UNIQUE KEY `id_Projet` (`id_Projet`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `t_projet`
--

INSERT INTO `t_projet` (`id_Projet`, `Nom_Projet`) VALUES
(1, 'Gestions de bases de données'),
(2, 'Création d\'une application console JAVA'),
(3, 'Mise à jour du site Web de la société'),
(4, 'Création d\'une interface graphique JAVA FX'),
(5, 'Réparation d\'une infrastructure réseau'),
(6, 'Déploiement d\'une solution applicative à destination des entreprises'),
(7, 'Digitalisation de la bureautique'),
(8, 'Création de site e-commerce'),
(9, 'Mis à jour d\'un forum'),
(11, 'Gestion de parcs informatiques'),
(12, 'ProjetLafleur');

-- --------------------------------------------------------

--
-- Structure de la table `t_solution`
--

DROP TABLE IF EXISTS `t_solution`;
CREATE TABLE IF NOT EXISTS `t_solution` (
  `id_Solution` int(10) NOT NULL AUTO_INCREMENT,
  `id_Incident` int(10) NOT NULL,
  `Nom_Solution` varchar(100) NOT NULL,
  `Temps` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id_Solution`),
  KEY `id_Incident` (`id_Incident`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `t_solution`
--

INSERT INTO `t_solution` (`id_Solution`, `id_Incident`, `Nom_Solution`, `Temps`) VALUES
(1, 1, 'Redémarrer les serveurs', NULL),
(2, 2, 'Refaire intégralement l\'installation', NULL),
(3, 3, 'Modifier les ports de connexion au serveur localhost', NULL),
(4, 4, 'Modifier une variable', NULL),
(5, 5, 'Changer les mots de passe de session', NULL),
(6, 6, 'Redémarrer la box internet', NULL),
(7, 4, 'Corriger une erreur de syntaxe', '');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `t_cahier_des_charges`
--
ALTER TABLE `t_cahier_des_charges`
  ADD CONSTRAINT `t_cahier_des_charges_ibfk_1` FOREIGN KEY (`id_Projet`) REFERENCES `t_projet` (`id_Projet`);

--
-- Contraintes pour la table `t_fichier`
--
ALTER TABLE `t_fichier`
  ADD CONSTRAINT `t_fichier_ibfk_1` FOREIGN KEY (`id_dossier`) REFERENCES `t_dossier` (`id`),
  ADD CONSTRAINT `t_fichier_ibfk_2` FOREIGN KEY (`id`) REFERENCES `t_fonction` (`id_Fonction`);

--
-- Contraintes pour la table `t_solution`
--
ALTER TABLE `t_solution`
  ADD CONSTRAINT `t_solution_ibfk_1` FOREIGN KEY (`id_Incident`) REFERENCES `t_incident` (`id_Incident`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
