<?php
/** 
 * Script de contr�le et d'affichage du cas d'utilisation "Ajouter"
 * @package default
 * @todo  RAS
 */
 
$repInclude = './include/';
$repVues = './vues/';

require($repInclude . "_init.inc.php");

$unIdF=lireDonneePost("idF", "");
$unid_fichier=lireDonneePost("id_fichier", ""); 
$unObjectif=lireDonneePost("objectif", "");
$unCodeF=lireDonneePost("codeF", "");
$unTemps=lireDonneePost("temps", "");

if (count($_POST)==0)
{
  $etape = 1;
}
else
{
  $etape = 2;
  ajouterFonction($unIdF, $unid_fichier, $unObjectif, $unCodeF, $unTemps, $tabErreurs);
}

// Construction de la page Rechercher
// pour l'affichage (appel des vues)
include($repVues."entete.php") ;
include($repVues."menu.php") ;
include($repVues ."erreur.php");
include($repVues."vAjouterFonction.php") ;
include($repVues."pied.php") ;
?>
  
